{
  "data_set_description": {
    "citation": "P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis. Modeling wine preferences by data mining from physicochemical properties. In Decision Support Systems, Elsevier, 47(4):547-553, 2009.",
    "collection_date": "2009-10-07",
    "contributor": "Leo Grin",
    "creator": "Paulo Cortez",
    "default_target_attribute": "quality",
    "description": "Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the \"regression on numerical features\" benchmark. Original description: \n \n**Author**: Tobias Kuehn  \n**Source**: Unknown - 2009  \n**Please cite**:   \n\n1. Title: Wine Quality \n\n2. Sources\nCreated by: Paulo Cortez (Univ. Minho), Antonio Cerdeira, Fernando Almeida, Telmo Matos and Jose Reis (CVRVV) @ 2009\n    \n3. Past Usage:\nP. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis. \nModeling wine preferences by data mining from physicochemical properties.\nIn Decision Support Systems, Elsevier, 47(4):547-553. ISSN: 0167-9236.\n\nIn the above reference, two datasets were created, using red and white wine samples.\nThe inputs include objective tests (e.g. PH values) and the output is based on sensory data (median of at least 3 evaluations made by wine experts). Each expert graded the wine quality between 0 (very bad) and 10 (very excellent). Several data mining methods were applied to model these datasets under a regression approach. The support vector machine model achieved the best results. Several metrics were computed: MAD, confusion matrix for a fixed error tolerance (T), etc. Also, we plot the relative importances of the input variables (as measured by a sensitivity analysis procedure).\n \n4. Relevant Information:\nThe two datasets are related to red and white variants of the Portuguese \"Vinho Verde\" wine. For more details, consult: http://www.vinhoverde.pt/en/ or the reference [Cortez et al., 2009]. Due to privacy and logistic issues, only physicochemical (inputs) and sensory (the output) variables  are available (e.g. there is no data about grape types, wine brand, wine selling price, etc.).\nThese datasets can be viewed as classification or regression tasks.\nThe classes are ordered and not balanced (e.g. there are munch more normal wines than excellent or poor ones). Outlier detection algorithms could be used to detect the few excellent or poor wines. Also, we are not sure if all input variables are relevant. So it could be interesting to test feature selection methods. \n\n5. Number of Instances: red wine - first 1599 instances; white wine - instances 1600 to 6497. \n \n6. Number of Attributes: 11 + output attribute\nNote: several of the attributes may be correlated, thus it makes sense to apply some sort of feature selection.\n\n7. Attribute information:\nFor more information, read [Cortez et al., 2009].\nInput variables (based on physicochemical tests):\n1 - fixed acidity\n2 - volatile acidity\n3 - citric acid\n4 - residual sugar\n5 - chlorides\n6 - free sulfur dioxide\n7 - total sulfur dioxide\n8 - density\n9 - pH\n10 - sulphates\n11 - alcohol\nOutput variable (based on sensory data): \n12 - quality (score between 0 and 10)\n\n8. Missing Attribute Values: None",
    "description_version": "1",
    "file_id": "22103099",
    "format": "arff",
    "id": "44011",
    "language": "English",
    "licence": "Public",
    "md5_checksum": "de37223c6d607dc30dd6ff814f02fd0b",
    "minio_url": "https://data.openml.org/datasets/0004/44011/dataset_44011.pq",
    "name": "wine_quality",
    "parquet_url": "https://data.openml.org/datasets/0004/44011/dataset_44011.pq",
    "processing_date": "2022-06-16 21:27:54",
    "status": "active",
    "upload_date": "2022-06-16T21:12:31",
    "url": "https://api.openml.org/data/v1/download/22103099/wine_quality.arff",
    "version": "5",
    "visibility": "public"
  }
}